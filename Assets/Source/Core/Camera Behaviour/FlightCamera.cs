﻿using System;
using System.Collections;
using UnityEngine;
using UniRx;

namespace CameraBehaviour
{
    /// <summary>
    /// Простое поведение свободного полета камеры.
    /// Используется UniRX для удобной замены корутинам. 
    /// </summary>
    public class FlightCamera : MonoBehaviour
    {
        [SerializeField] float speed = 10f;
        [SerializeField] float sensitivity = 5f;

        IDisposable _flightUpdater;

        public void EnableFlightMode()
        {
            _flightUpdater = Observable.EveryUpdate()
                .Subscribe(_ => UpdateFlight());
        }

        public void DisableFlightMode()
        {
            _flightUpdater?.Dispose();
            _flightUpdater = null;
        }

        private void UpdateFlight()
        {
            var direction = GetDirection(transform.forward, transform.right);
            transform.position += direction * speed * Time.deltaTime;

            transform.eulerAngles += GetRotation() * sensitivity;
        }

        private Vector3 GetDirection(Vector3 forward, Vector3 right)
            => forward * Input.GetAxis("Vertical") + right * Input.GetAxis("Horizontal");

        private Vector3 GetRotation()
            => new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);
    }
}