﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Connectables
{
    /// <summary>
    /// Класс, описывающий общий объект Connectable
    /// для работы с отдельными её составными частями (платформа и коннеткор).
    /// </summary>
    public class Connectable : MonoBehaviour
    {
        MovingPlatform _platform;
        Connector _connector;

        private void Init()
        {
            _platform = gameObject.GetComponentInChildren<MovingPlatform>();
            _connector = gameObject.GetComponentInChildren<Connector>();

            _platform.Init();
            _connector.Init();
        }

        public Connector Connector => _connector;

        /// <summary>
        /// Шаблон "Фабрика" для удобного создания экземпляра Connectable.
        /// </summary>
        public class Factory
        {
            Connectable _prefab;
            Transform _group;

            public Factory(Connectable prefab)
            {
                _group = new GameObject("Group Connectables").transform;
                _prefab = prefab;
            }

            public Connectable Create(Vector3 position, Quaternion rotation)
            {
                var connectable = GameObject.Instantiate<Connectable>(_prefab, position, rotation, _group);
                connectable.Init();

                return connectable;
            }
        }
    }
    
}