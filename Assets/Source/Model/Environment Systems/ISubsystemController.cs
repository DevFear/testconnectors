﻿using Cysharp.Threading.Tasks;

namespace EnvironmentSystems
{
    /// <summary>
    /// Интерфейс для реализации контроллера независимой подсистемы.
    /// </summary>
    interface ISubsystemController
    {
        /// <summary>
        /// Позволяет контроллеру взять на себя ответстенность за выполнение отдельной подсистемы.
        /// </summary>
        /// <returns></returns>
        UniTask TransferControl();
    }

    interface ISubsystemController<T>
    {
        UniTask TransferControl(T data);
    }
}